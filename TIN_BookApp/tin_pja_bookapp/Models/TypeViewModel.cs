﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace tin_pja_bookapp.Models
{
    public class TypeViewModel
    {

        public TypeViewModel()
        {
            this.Books = new HashSet<BookViewModel>();
        }

        [Key]
        public int IdType { get; set; }

        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        public virtual ICollection<BookViewModel> Books { get; set; }

    }
}