namespace tin_pja_bookapp.DAL.BookMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ArticleViewModels",
                c => new
                    {
                        IdArticle = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.IdArticle);
            
            CreateTable(
                "dbo.AuthorViewModels",
                c => new
                    {
                        IdAuthor = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.IdAuthor);
            
            CreateTable(
                "dbo.BookViewModels",
                c => new
                    {
                        IdBook = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        Description = c.String(),
                        Image = c.Binary(),
                    })
                .PrimaryKey(t => t.IdBook);
            
            CreateTable(
                "dbo.TypeViewModels",
                c => new
                    {
                        IdType = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.IdType);
            
            CreateTable(
                "dbo.TypeViewModelBookViewModels",
                c => new
                    {
                        TypeViewModel_IdType = c.Int(nullable: false),
                        BookViewModel_IdBook = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.TypeViewModel_IdType, t.BookViewModel_IdBook })
                .ForeignKey("dbo.TypeViewModels", t => t.TypeViewModel_IdType, cascadeDelete: true)
                .ForeignKey("dbo.BookViewModels", t => t.BookViewModel_IdBook, cascadeDelete: true)
                .Index(t => t.TypeViewModel_IdType)
                .Index(t => t.BookViewModel_IdBook);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TypeViewModelBookViewModels", "BookViewModel_IdBook", "dbo.BookViewModels");
            DropForeignKey("dbo.TypeViewModelBookViewModels", "TypeViewModel_IdType", "dbo.TypeViewModels");
            DropIndex("dbo.TypeViewModelBookViewModels", new[] { "BookViewModel_IdBook" });
            DropIndex("dbo.TypeViewModelBookViewModels", new[] { "TypeViewModel_IdType" });
            DropTable("dbo.TypeViewModelBookViewModels");
            DropTable("dbo.TypeViewModels");
            DropTable("dbo.BookViewModels");
            DropTable("dbo.AuthorViewModels");
            DropTable("dbo.ArticleViewModels");
        }
    }
}
