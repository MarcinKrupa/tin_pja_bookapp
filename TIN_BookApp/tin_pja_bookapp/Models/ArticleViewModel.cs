﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace tin_pja_bookapp.Models
{
    public class ArticleViewModel
    {
    
        [Key]
        public int IdArticle { get; set; }

        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        [MaxLength(5000)]
        public string Description { get; set; }

    }
}