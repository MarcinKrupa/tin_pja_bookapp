﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using tin_pja_bookapp.DAL;
using tin_pja_bookapp.Models;

namespace tin_pja_bookapp.Controllers
{
    public class ArticleController : Controller
    {
        private BooksDb db = new BooksDb();

        // GET: Article
        public ActionResult Index()
        {
            return View(db.Articles.ToList());
        }

        // GET: Article/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ArticleViewModel articleViewModel = db.Articles.Find(id);
            if (articleViewModel == null)
            {
                return HttpNotFound();
            }
            return View(articleViewModel);
        }

        // GET: Article/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Article/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdArticle,Name,Description")] ArticleViewModel articleViewModel)
        {
            if (ModelState.IsValid)
            {
                db.Articles.Add(articleViewModel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(articleViewModel);
        }

        // GET: Article/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ArticleViewModel articleViewModel = db.Articles.Find(id);
            if (articleViewModel == null)
            {
                return HttpNotFound();
            }
            return View(articleViewModel);
        }

        // POST: Article/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdArticle,Name,Description")] ArticleViewModel articleViewModel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(articleViewModel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(articleViewModel);
        }

        // GET: Article/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ArticleViewModel articleViewModel = db.Articles.Find(id);
            if (articleViewModel == null)
            {
                return HttpNotFound();
            }
            return View(articleViewModel);
        }

        // POST: Article/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ArticleViewModel articleViewModel = db.Articles.Find(id);
            db.Articles.Remove(articleViewModel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
