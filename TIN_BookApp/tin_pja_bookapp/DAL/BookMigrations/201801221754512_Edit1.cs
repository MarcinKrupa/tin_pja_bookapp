namespace tin_pja_bookapp.DAL.BookMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Edit1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BookViewModels", "CurrentIdAuthor", c => c.Int(nullable: false));
            CreateIndex("dbo.BookViewModels", "CurrentIdAuthor");
            AddForeignKey("dbo.BookViewModels", "CurrentIdAuthor", "dbo.AuthorViewModels", "IdAuthor", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BookViewModels", "CurrentIdAuthor", "dbo.AuthorViewModels");
            DropIndex("dbo.BookViewModels", new[] { "CurrentIdAuthor" });
            DropColumn("dbo.BookViewModels", "CurrentIdAuthor");
        }
    }
}
