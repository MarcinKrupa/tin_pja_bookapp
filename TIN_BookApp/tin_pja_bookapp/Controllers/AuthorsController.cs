﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using tin_pja_bookapp.DAL;
using tin_pja_bookapp.Models;

namespace tin_pja_bookapp.Controllers
{
    public class AuthorsController : Controller
    {
        private BooksDb db = new BooksDb();

        // GET: Authors
        public ActionResult Index()
        {
            return View(db.Authors.ToList());
        }

        // GET: Authors/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AuthorViewModel authorViewModel = db.Authors.Find(id);
            if (authorViewModel == null)
            {
                return HttpNotFound();
            }
            return View(authorViewModel);
        }

        // GET: Authors/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Authors/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdAuthor,Name,Description")] AuthorViewModel authorViewModel)
        {
            if (ModelState.IsValid)
            {
                db.Authors.Add(authorViewModel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(authorViewModel);
        }

        // GET: Authors/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AuthorViewModel authorViewModel = db.Authors.Find(id);
            if (authorViewModel == null)
            {
                return HttpNotFound();
            }
            return View(authorViewModel);
        }

        // POST: Authors/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdAuthor,Name,Description")] AuthorViewModel authorViewModel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(authorViewModel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(authorViewModel);
        }

        // GET: Authors/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AuthorViewModel authorViewModel = db.Authors.Find(id);
            if (authorViewModel == null)
            {
                return HttpNotFound();
            }
            return View(authorViewModel);
        }

        // POST: Authors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            AuthorViewModel authorViewModel = db.Authors.Find(id);
            db.Authors.Remove(authorViewModel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
