﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace tin_pja_bookapp.Models
{
    public class BookViewModel
    {
        public BookViewModel()
        {
            this.Types = new HashSet<TypeViewModel>();
        }

        [Key]
        public int IdBook { get; set; }

        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        [MaxLength(5000)]
        public string Description { get; set; }

        public byte[] Image { get; set; }

        // many to many
        public virtual ICollection<TypeViewModel> Types { get; set; }

        // one to many
        public int CurrentIdAuthor { get; set; }

        public AuthorViewModel CurrentAuthor { get; set; }
    }
}