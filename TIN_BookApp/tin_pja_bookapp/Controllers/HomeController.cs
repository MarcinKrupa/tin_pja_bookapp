﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using tin_pja_bookapp.DAL;

namespace tin_pja_bookapp.Controllers
{
    public class HomeController : Controller
    {
        private BooksDb db = new BooksDb();

        // GET: Article
        public ActionResult Index()
        {
            return View(db.Articles.Take(3).ToList());
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}