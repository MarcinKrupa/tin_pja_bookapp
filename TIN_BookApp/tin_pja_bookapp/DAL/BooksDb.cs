﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using tin_pja_bookapp.Models;

namespace tin_pja_bookapp.DAL
{
    public class BooksDb : DbContext
    {

        public BooksDb()
            : base("DefaultConnection")
        {      
        }

        public DbSet<BookViewModel> Books { get; set; }

        public DbSet<AuthorViewModel> Authors { get; set; }

        public DbSet<ArticleViewModel> Articles { get; set; }

        public DbSet<TypeViewModel> Types { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<BookViewModel>()
            .HasRequired<AuthorViewModel>(s => s.CurrentAuthor)
            .WithMany(g => g.Books)
            .HasForeignKey<int>(s => s.CurrentIdAuthor);

            modelBuilder.Entity<BookViewModel>()
                        .HasMany<TypeViewModel>(s => s.Types)
                        .WithMany(c => c.Books)
                        .Map(cs =>
                        {
                            cs.MapLeftKey("BookId");
                            cs.MapRightKey("TypeId");
                            cs.ToTable("BookType");
                        });
        }

    }

}