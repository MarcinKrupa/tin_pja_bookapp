﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using tin_pja_bookapp.DAL;
using tin_pja_bookapp.Models;

namespace tin_pja_bookapp.Controllers
{
    public class BooksController : Controller
    {
        private BooksDb db = new BooksDb();

        // GET: Books
        public ActionResult Index()
        {
            var books = db.Books.Include(b => b.CurrentAuthor);

            return View(books.ToList());
        }

        // GET: Books/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            BookViewModel book = db.Books
              .Include(i => i.CurrentAuthor)
              .SingleOrDefault(x => x.IdBook == id);

            if (book == null)
            {
                return HttpNotFound();
            }
            return View(book);
        }

        // GET: Books/Create
        public ActionResult Create()
        {
            ViewBag.CurrentIdAuthor = new SelectList(db.Authors, "IdAuthor", "Name");
            return View();
        }

        // POST: Books/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdBook,Name,Description,Image,CurrentIdAuthor")] BookViewModel bookViewModel)
        {
            if (ModelState.IsValid)
            {
                db.Books.Add(bookViewModel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CurrentIdAuthor = new SelectList(db.Authors, "IdAuthor", "Name", bookViewModel.CurrentIdAuthor);
            return View(bookViewModel);
        }

        // GET: Books/Edit/5
        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BookViewModel bookViewModel = db.Books.Find(id);
            if (bookViewModel == null)
            {
                return HttpNotFound();
            }
            ViewBag.CurrentIdAuthor = new SelectList(db.Authors, "IdAuthor", "Name", bookViewModel.CurrentIdAuthor);
            return View(bookViewModel);
        }

        // POST: Books/Edit/5
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdBook,Name,Description,Image,CurrentIdAuthor")] BookViewModel bookViewModel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bookViewModel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CurrentIdAuthor = new SelectList(db.Authors, "IdAuthor", "Name", bookViewModel.CurrentIdAuthor);
            return View(bookViewModel);
        }

        // GET: Books/Delete/5
        [Authorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BookViewModel book = db.Books
              .Include(i => i.CurrentAuthor)
              .SingleOrDefault(x => x.IdBook == id);

            if (book == null)
            {
                return HttpNotFound();
            }
            return View(book);
        }

        // POST: Books/Delete/5
        [Authorize]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BookViewModel bookViewModel = db.Books.Find(id);
            db.Books.Remove(bookViewModel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
