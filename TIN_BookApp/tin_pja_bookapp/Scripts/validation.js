﻿function validateForm() {
    var mark = document.forms["valid"]["mark"].value;

    if (mark == null || mark == "" || !mark.trim().length) {
        alert("Nie można wysłać pustej oceny!");
        return false;
    }

    if (isNaN(mark)) {
        alert("Ocena musi być cyfrą!");
    }

    if (mark > 10 || mark < 0) {
        alert("Ocena nie mieści się w ramach!");
        return false;
    }

    return true;
}

function validateCRUD() {
    var str = document.forms["valid"]["str"].value;

    if (str == null || str == "" || !str.trim().length) {
        alert("Nie można wysłać pustej nazwy!");
        return false;
    }

    return true;
}

function validateEmail() {
    var email = document.forms["valid"]["email"].value;
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

    if (email == null || email == "" || !email.trim().length) {
        alert("Nie można wysłać pustej nazwy!");
        return false;
    }

    if (reg.test(email) == false) {
        alert("Invalid email");
        return false;
    }

    return true;
}