﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(tin_pja_bookapp.Startup))]
namespace tin_pja_bookapp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
