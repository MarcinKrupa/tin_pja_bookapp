﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using tin_pja_bookapp.DAL;
using tin_pja_bookapp.Models;

namespace tin_pja_bookapp.Controllers
{
    public class TypeController : Controller
    {
        private BooksDb db = new BooksDb();

        // GET: Type
        public ActionResult Index()
        {
            return View(db.Types.ToList());
        }

        // GET: Type/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TypeViewModel typeViewModel = db.Types.Find(id);
            if (typeViewModel == null)
            {
                return HttpNotFound();
            }
            return View(typeViewModel);
        }

        // GET: Type/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Type/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdType,Name")] TypeViewModel typeViewModel)
        {
            if (ModelState.IsValid)
            {
                db.Types.Add(typeViewModel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(typeViewModel);
        }

        // GET: Type/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TypeViewModel typeViewModel = db.Types.Find(id);
            if (typeViewModel == null)
            {
                return HttpNotFound();
            }
            return View(typeViewModel);
        }

        // POST: Type/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdType,Name")] TypeViewModel typeViewModel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(typeViewModel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(typeViewModel);
        }

        // GET: Type/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TypeViewModel typeViewModel = db.Types.Find(id);
            if (typeViewModel == null)
            {
                return HttpNotFound();
            }
            return View(typeViewModel);
        }

        // POST: Type/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TypeViewModel typeViewModel = db.Types.Find(id);
            db.Types.Remove(typeViewModel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
