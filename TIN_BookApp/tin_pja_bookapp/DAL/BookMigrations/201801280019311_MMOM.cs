namespace tin_pja_bookapp.DAL.BookMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MMOM : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.TypeViewModelBookViewModels", newName: "BookType");
            RenameColumn(table: "dbo.BookType", name: "TypeViewModel_IdType", newName: "TypeId");
            RenameColumn(table: "dbo.BookType", name: "BookViewModel_IdBook", newName: "BookId");
            RenameIndex(table: "dbo.BookType", name: "IX_BookViewModel_IdBook", newName: "IX_BookId");
            RenameIndex(table: "dbo.BookType", name: "IX_TypeViewModel_IdType", newName: "IX_TypeId");
            DropPrimaryKey("dbo.BookType");
            AddPrimaryKey("dbo.BookType", new[] { "BookId", "TypeId" });
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.BookType");
            AddPrimaryKey("dbo.BookType", new[] { "TypeViewModel_IdType", "BookViewModel_IdBook" });
            RenameIndex(table: "dbo.BookType", name: "IX_TypeId", newName: "IX_TypeViewModel_IdType");
            RenameIndex(table: "dbo.BookType", name: "IX_BookId", newName: "IX_BookViewModel_IdBook");
            RenameColumn(table: "dbo.BookType", name: "BookId", newName: "BookViewModel_IdBook");
            RenameColumn(table: "dbo.BookType", name: "TypeId", newName: "TypeViewModel_IdType");
            RenameTable(name: "dbo.BookType", newName: "TypeViewModelBookViewModels");
        }
    }
}
