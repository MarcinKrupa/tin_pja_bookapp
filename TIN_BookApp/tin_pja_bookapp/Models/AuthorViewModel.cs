﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace tin_pja_bookapp.Models
{
    public class AuthorViewModel
    {

        [Key]
        public int IdAuthor { get; set; }

        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        [MaxLength(5000)]
        public string Description { get; set; }

        // one to many
        public ICollection<BookViewModel> Books { get; set; }

    }
}